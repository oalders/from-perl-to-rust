# From Perl to Rust

This repo contains the source to [From Perl to Rust](https://oylenshpeegul.gitlab.io/from-perl-to-rust/), a guide to learning Rust for folks who already know Perl.

## Building the guide locally

Install [mdBook](https://github.com/rust-lang/mdBook) and run '`mdbook build`' or '`mdbook serve`' in the project directory.

## Building the guide online

The guide is [built automatically when pushing to GitLab](https://yethiel.gitlab.io/post/mdbook-with-gitlab-ci-cd/).
