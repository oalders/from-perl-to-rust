# Generic Types

Rust also supports the notion of generic types. We can write code that supports a number of types by using a stand in, often just the letter T. This includes when defining structures and enumerations.

Rust does not have higher-kinded types[^note]. If you don't know what that means, don't worry about it. If you do, then you might be looking for them. You can stop.

___
[^note]: In [Rust 1.65.0](https://github.com/rust-lang/rust/releases/tag/1.65.0), generic associated types were stabilized. These are sort of generic over something that is itself generic, so it's rubbing up against one aspect of higher-kinded types.

