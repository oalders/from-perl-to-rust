# Hello, Rust!

Let's start with the "hello world" program! This was originated by Brian Kernighan some 40 years ago. It's a brilliant piece of pedagogy in C. It's less interesting in Perl and Rust, but it will serve our purposes nonetheless. The "hello world" program in Perl might look like this

```bash
perl -E 'say "Hello, World!"'
```

Here we call `perl` directly, feeding it our Perl source code to be immediately interpreted. Alternatively, we could put our code in a file

```perl
#!/usr/bin/env perl

use v5.28;
use warnings;

say "Hello, World!";
```

and then feed that file to `perl`. If the file were called `hello`, then we could call `perl` directly

```
$ perl hello
Hello, World!
```

or through the shebang line

```
$ ./hello
Hello, World!
```

In Rust, the "hello world" program looks like this

```rust
fn main() {
    println!("Hello, World!");
}
```

To run it, we must first *compile* it. If it were in a file called `hello.rs,` we could call the rust compiler directly with

```
$ rustc hello.rs
```

This would create another file, `hello` which is an executable

```
$ ./hello
Hello, World!
```

Our Rust source code is no longer required. We have a stand-alone executable, specific to our machine.

```
$ file hello
hello: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=05fddacb48ac6fa3ac26ff897c61f31941a04c4d, with debug_info, not stripped
```

However, if we wanted it to run on some other machine, we might have to recompile it for that platform.

Contrast that with Perl. Our Perl source code can be run by any `perl` on any platform. Perl itself is a big ole C program that must be compiled to a `perl` executable for each platform. But once we have it (often it comes with the operating system, so we don't even have to compile it), we can run any Perl code from anywhere.

![Compile and run string diagram](hello.svg "compile and run")

So that's the first big difference in our workflow. We run Perl immediately as we are developing, but we must first compile our Rust.

