# Tests

In Perl, we put our tests in a `t` directory and then `prove -l` will run them. Similarly, Rust has a `tests` directory and `cargo test` will run the tests there. 

| Rust           | Perl              |
|----------------|-------------------|
| tests          | t                 |
| `cargo test`   | `prove -l`        |
| `use testing;` | `use Test::Most;` |

But Rust does this mostly for integration tests. Unit tests are typically put right in the same file as the code.

For example, I wrote a Perl module called [Time::Moment::Epoch](https://metacpan.org/pod/Time::Moment::Epoch) which converts a bunch of different epoch times to [Time::Moment](https://metacpan.org/pod/Time::Moment) times. It has a bunch of unit tests in [t/Time-Moment-Epoch.t](https://github.com/oylenshpeegul/Time-Moment-Epoch/blob/master/t/Time-Moment-Epoch.t).

I wrote a similar Rust crate called [epochs](https://crates.io/crates/epochs), which converts those same epoch times to [chrono::NaiveDateTime](https://docs.rs/chrono/0.4.19/chrono/naive/struct.NaiveDateTime.html) times. It has similar unit tests [at the bottom of the library file itself](https://github.com/oylenshpeegul/Epochs-rust/blob/master/src/lib.rs#L439).

The `#[cfg(test)]` macro tells cargo to run those tests when it is called as `cargo test`, but to ignore them when it is called as `cargo build` or `cargo run`.
