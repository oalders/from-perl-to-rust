# Types

Perl is dynamically typed. Rust is statically typed. In practice, I think this is the biggest difference for the programmer. In general, we have to be much more aware of the types of variables in Rust than we do in Perl.

Also, Perl is weakly typed, whereas Rust is strongly typed. 

| Rust   | Perl    |
|--------|---------|
| static | dynamic |
| strong | weak    |

That means that Rust is not going to do any casting or automatic conversion of types the way that Perl does.

Turning that table inside-out, we see that Rust and Perl couldn't be more different

|            | static | dynamic |
|------------|--------|---------|
| **strong** | Rust   |         |
| **weak**   |        | Perl    |

Let's fill in the rest of the table with other languages you may know

|            | static | dynamic |
|------------|--------|---------|
| **strong** | Rust   | Python  |
| **weak**   | C      | Perl    |

Python is dynamic, like Perl, but its types are pretty strong. For example, we can't just use the string "3" as a number in Python as we do in Perl; we have to convert it first.

```bash
$ perl -E 'say "3" + 4'
7

$ python -c 'print("3" + 4)'
Traceback (most recent call last):
  File "<string>", line 1, in <module>
TypeError: cannot concatenate 'str' and 'int' objects

$ python -c 'print(int("3") + 4)'
7
```

C is static, like Rust, but its types are very weak. You want to add a character to an integer? Have at it!

```c
    char c = '3';
    int i = 4;

    printf("%d\n", c + i);
```

This may not print what you expected, but it will compile and print something.

So Rust types are both static, like C, and strong, like Python? That sounds like it might be a pain to use. And indeed Rust is a good deal more finicky than Perl. It will take some getting used to for sure.

The good news is, Rust has a really nice type system! Unlike the type systems found in most imperative programming languages, Rust has *algebraic data types* --- something usually only found in functional programming languages.

Rust also has type inference, so we can often leave off the type and Rust will figure it out.

These two properties combined mean Rust's types become more of a tool we use, rather than a burden we endure.

